#!/usr/bin/env python
import os
import sys
Fjoin = os.path.join
list_files, variables = [], []
count_func, count_class, count_module = 0, 0, 0
for dirpath, dirnames, filenames in os.walk(sys.argv[1]):
    list_files.extend([Fjoin(dirpath, files) for files in filenames])
for file_ in list_files:
    if file_.endswith('.py') or file_.endswith('.py~'):
        count_module += 1
        try:
            f = open(file_)
        except:
            pass
        for line in f:
            if line.strip().startswith('#') or line.strip().startswith('"""'):
                pass
            else:
                if line.strip().startswith('def') and line.strip().endswith(':'):
                    count_func += 1
                elif line.startswith('class') and line.strip().endswith(':'):
                    count_class += 1
                elif "=" in line:
                    if '>>>' in line or '==' in line or '!=' in line:
                        pass
                    else:
                        try:
                            x = line.strip().split('=')[0]
                        except:
                            pass
                        variables.append(x)
li_al = list(set(variables))
li_num = [variables.count(i) for i in li_al]
list_used = list(set(al for al in li_al for num in sorted(li_num)[-10:] if (variables.count(al) == num)))
f.close()
print "Numbers of functions =", count_func
print "Numbers of classes =", count_class
print "Numbers of modules =", count_module
print "Top 10 most used variables =", list_used